/* Generated automatically. */
static const char configuration_arguments[] = "../../gcc-4.8.2/configure --enable-languages=c,c++,objc,obj-c++ --with-gnu-as --with-gnu-ld --with-gcc --with-march=armv4t --enable-poison-system-directories --enable-interwork --enable-multilib --disable-dependency-tracking --disable-threads --disable-win32-registry --disable-nls --disable-debug --disable-libmudflap --disable-libssp --disable-libgomp --disable-libstdcxx-pch --target=arm-none-eabi --with-newlib --with-headers=../../newlib-2.1.0/newlib/libc/include --prefix=/opt/devkitpro/devkitARM --enable-lto --with-bugurl=http://wiki.devkitpro.org/index.php/Bug_Reports --with-pkgversion='devkitARM release 42'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
